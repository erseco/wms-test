<!DOCTYPE html>
<html>
<body>

<h1>WMS TEST PAGE</h1>

<h2>COMMIT:<?php echo getenv('COMMIT'); ?></h2>

<p>This is a test page for WMS</p>

<ul>
	<li><a href="info.php">PHP Info page</a></li>
	<li><a href="postgres.php">PostgreSQL Info page</a></li>
	<li><a href="elastic.php">ElasticSearch Info page</a></li>
	<li><a href="http://localhost:5061">Kibana</a></li>
</ul>
</body>
</html>