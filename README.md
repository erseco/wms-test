# Pruebas de nivel WMS

## 1-docker-simple

Aquí se ha creado una imagen con la base alpine:3.10, en la misma se han instalado tanto PHP como NGINX así como los módulos PHP indicados, se le ha inyectado una configuración de NGINX y puesto el código PHP dentro de la ruta `/app`.

El tamaño de este contenedor es de 7 megabytes.

Para poder "orquestar" esto se ha creado un fichero `docker-compose.yml` con los diferentes servicios.

Para poder probarlo en local:
```
docker-compose up -d
```
Y accedemos a http://localhost para ver la web o a http://localhost:5061 para entrar a Kibana


## 2-docker-services

Aquí se ha definido una imagen con la base php-fpm:7.2-alpine, para instalar los paquetes necesarios se ha utilizado el script `docker-php-ext-install` que se incluye en la imagen base, para optimizar el tamaño una vez configurados los módulos se han eliminado las librerías de desarrollo. También aquí se ha puesto dentro el código PHP dentro de la ruta `/app`.

El tamaño de este contenedor es de 34 megabytes.

Para poder "orquestar" esto se ha creado un fichero `docker-compose.yml` con los diferentes servicios. En este caso NGINX y PHP funcionan por separado, por lo que NGINX envía todas las peticiones al contenedor con PHP a través del puerto 9000.

Para poder probarlo en local:
```
docker-compose up -d
```
Y accedemos a http://localhost para ver la web o a http://localhost:5061 para entrar a Kibana


## 2-Kubernetes

Una vez Realizadas las dos aproximaciones con contenedores hemos creado un fichero `deployment.yml` que define un Pod con los contenedores, se pueden utilizar las dos aproximaciones anterior (de hecho está comentada una de ellas para poder intercambiarse).

Dicho Pod inyecta los archivos de configuración de NGIX y Filebeat mediante un `configMap`, además le hemos definido que fuerce a hacer un pull del contenedor con el código PHP cada vez que se apliquen los cambios. Para multiplexar las salidas hemos utilizado Ingress sobre el dominio `ops.ernestoserrano.com`.

Se pueden crear todos los despliegues con el nombre de la rama que se desee, por defecto tenemos estos:

- http://master.ops.ernestoserrano.com/ (Por el nombre de la rama)
- http://preproduction.ops.ernestoserrano.com/
- http://production.ops.ernestoserrano.com/

## Sobre el .gitlab-ci.yml

Para compilar y hacer auto-deploy se han utilizado los pipelines de GitLab-CI, se ha configurado para que se haga en tres `stages`, **build**, **test** y **deploy**. 

Para que la construción de los contenedores sea más rapida primero se intenta descargar (si existe), una imagen con el mismo nombre.

Para aprovechar lo construido entre `stages` se han utilizado los _artifacts_ que proporciona GitLab-CI

Solo pusheará la nueva imagen si pasa los test satisfactoriamente, para esto se ha agregado el programa `phplint` para que valide si el código PHP es correcto.

Una vez pusheados los nuevos contenedores pasamos a la fase de deploy, que creara/refrescara el despliegue que tenga la rama indicada.

En la rama `master` hay dos jobs manuales que despliegan el codigo de master en preproduction y en production

En las ramas distintas de master hay un job manual que permite hacer un borrado del desplieuge.

### Comentarios adicionales

- Aunque el ejercicio especifica la ruta `/var/logs/app/wms-*.log` se ha utilizado `/var/log/`ya que es el definido en el estándar Filesystem Hierarchy Standard http://www.pathname.com/fhs/pub/fhs-2.3.html
- Se ha agregado un contenedor con Kibana para poder visualizar los datos recogidos por Filebeat.
- Se han desarrollado unas simples páginas en PHP que consultan el estado de cada servicio.
- Por simplificar la entrega se ha hecho todo en un único repositorio, esto ha resultado en quizá un .gitlab-ci.yml un poco más complejo de entender.
- Se ha creado un fichero `.dockerignore` para evitar copiar elementos innecesarios dentro de las imágenes.
- No utilizo la imagen Alpine de PostgreSQL porque da problemas al ordenar registros por una limitación de la libreri musl.
- Como quería que el sistema funcionara tanto con docker-compose como con Kubernetes se ha descomentado la opción el `clear_env = no` en la configuración de PHP, esto se ha hecho al vuelo con el comando `sed`. De esta forma podemos acceder por el nombre del host en docker-compose y por localhost en Kubernetes.
- Me hubiera gustado terminar de configurar toda la parte de generación de certificados SSL con Let's Encrypt pero no quería teneros esperando más tiempo.
- A producción yo sólo desplegaría `tags`, así se puede volver a un punto anterior en caso de algún problema.



